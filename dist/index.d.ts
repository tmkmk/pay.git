import { PayParm } from "./itf";
import { ORDER, REQUEST } from "./struct";
export { ORDER, REQUEST };
import YL from "./yl";
import WX from "./wx";
import AL from "./al";
export declare class PAY {
    al: AL;
    wx: WX;
    yl: YL;
    constructor(config: PayParm);
}
