import * as itf from "./itf";
export declare class ORDER {
    money: itf.ns;
    title: string;
    order: string;
    backUrl?: string;
    realPay?: itf.fun;
    [prop: string]: any;
    txnTime?: string;
    payTimeout?: string;
    accessType?: string;
    frontUrl?: string;
    channelType?: string;
    txnSubType?: string;
    ip?: string;
    tradeType?: string;
    openid?: string;
    orderT?: string;
    orderM?: itf.ns;
    constructor(param: itf.oa);
}
export declare class REQUEST {
    order: string;
    payment: number;
    queryId?: string;
    url?: string;
    body?: string;
    readonly err?: any;
    check: boolean;
    raw: any;
    [k: string]: any;
    constructor(p: any);
    private toNumber;
}
