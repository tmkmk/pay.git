/** 详细看接口备注
 */
export interface Create {
    appId: string;
    mchId: string;
    partnerKey: string;
    notifyUrl?: string;
    private: {
        id?: string;
        cert: string;
        key: string;
    };
}
export interface Config {
    partner_key: string;
    notify_url: string;
    public: {
        appid: string;
        mch_id: string;
    };
    private: {
        id?: string;
        cert: string;
        key: string;
    };
}
