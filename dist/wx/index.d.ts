import { oa } from "../itf";
import { ORDER, REQUEST } from "../struct";
import * as itf from "./itf";
export declare class PAY {
    type: string[];
    config: itf.Config;
    constructor(param: itf.Create);
    private sign;
    private buildXML;
    private parseXML;
    private validate;
    private ret;
    create(p: ORDER): Promise<REQUEST>;
    query(order: string): Promise<REQUEST>;
    close(order: string): Promise<REQUEST>;
    back(param: any): Promise<REQUEST>;
    notify(req: oa): Promise<REQUEST>;
    front(order: string): Promise<REQUEST>;
}
export default PAY;
