"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PAY = void 0;
const method = __importStar(require("@mac-xiang/method"));
const axios_1 = __importDefault(require("axios"));
const form_data_1 = __importDefault(require("form-data"));
const alipay_sdk_1 = __importDefault(require("alipay-sdk"));
const form_1 = __importDefault(require("alipay-sdk/lib/form"));
const cheerio_1 = __importDefault(require("cheerio"));
const struct_1 = require("../struct");
class PAY {
    constructor(param) {
        this.type = ["NATIVE", "JSAPI", "APP", "NWEB"];
        this.config = param;
        this.alipay = new alipay_sdk_1.default({
            appId: this.config.appId,
            privateKey: this.config.privateKey,
            alipayPublicKey: this.config.publicKey,
            gateway: this.config.gateway,
            charset: "utf-8",
            version: "1.0",
            signType: "RSA2",
            encryptKey: this.config.encryptKey,
        });
    }
    Request(p, order) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            const ch = cheerio_1.default.load(""), dom = ch.parseHTML(p);
            const dr = dom[dom.findIndex((a) => { return a.name == "form"; })];
            const form = new form_data_1.default();
            const body = {};
            dr.childNodes.filter((a) => {
                return a.name == "input";
            }).forEach((a) => {
                form.append(a.attribs.name, a.attribs.value);
                try {
                    body[a.attribs.name] = JSON.parse(a.attribs.value);
                }
                catch (error) {
                    body[a.attribs.name] = a.attribs.value;
                }
            });
            const ret = (a) => {
                const p = {};
                if (!!order)
                    p.order = order;
                const r = new struct_1.REQUEST(Object.assign(p, a));
                resolve(r);
            };
            if (dr.attribs && typeof dr.attribs.action == "string") {
                axios_1.default({
                    method: "post",
                    url: dr.attribs.action,
                    data: form,
                    headers: form.getHeaders()
                }).then(r => {
                    const d = r.data.alipay_trade_query_response ? r.data.alipay_trade_query_response : r.data;
                    ret(d.alipay_trade_close_response ||
                        d.alipay_trade_refund_response ||
                        d);
                }).catch(e => {
                    ret({ err: "网络错误", raw: e });
                });
            }
            else
                ret({ err: "解析错误", raw: p });
        }));
    }
    orderMethod(p) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            const msd = p.method;
            delete p.method;
            if (p.order) {
                p.out_trade_no = p.order;
                delete p.order;
            }
            const formData = new form_1.default();
            formData.addField('bizContent', p);
            const html = yield this.alipay.exec("alipay.trade." + msd, {}, { formData });
            let a = { err: "网络错误", raw: html };
            if (typeof html == "string") {
                a = yield this.Request(html, p.out_trade_no);
            }
            const r = new struct_1.REQUEST(a);
            resolve(r);
        }));
    }
    create(p) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            const app = p.trade_type == "APP";
            const q = {
                OutTradeNo: p.order,
                ProductCode: app ? "QUICK_MSECURITY_PAY" : "FAST_INSTANT_TRADE_PAY",
                TotalAmount: parseInt(p.money.toString()) / 100,
                Subject: p.title,
                TimeoutExpress: "5m",
                NotifyUrl: p.backUrl ? p.backUrl : this.config.notifyUrl, // NotifyUrl notify_url
                // Body
            };
            if (p.pack_params)
                q.pack_params = JSON.stringify(p.pack_params);
            const formData = new form_1.default();
            // formData.addField('notifyUrl', q.NotifyUrl);
            formData.addField('notifyUrl', app ? "" : q.NotifyUrl);
            formData.addField('bizContent', q);
            if (app)
                formData.setMethod("get");
            let ret = { err: "下单错误" };
            try {
                const result = yield this.alipay.exec(`alipay.trade.${app ? "app" : "page"}.pay`, {}, { formData });
                ret = {
                    order: q.OutTradeNo,
                    money: parseInt(p.money.toString()),
                    respCode: "00",
                    body: result
                };
                if (app && typeof result == "string") {
                    ret.app = result.substr(result.indexOf("?") + 1);
                }
            }
            catch (error) {
                ret = { err: "网络错误", raw: error };
            }
            resolve(new struct_1.REQUEST(ret));
        }));
    }
    ;
    query(order) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.orderMethod({
                out_trade_no: order,
                method: "query"
            });
        });
    }
    ;
    close(order) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.orderMethod({
                out_trade_no: order,
                method: "close"
            });
        });
    }
    ;
    back(p) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            let ret = { err: "参数错误", raw: p };
            const rr = (a, b) => {
                const r = new struct_1.REQUEST(a ? (b ? Object.assign(b, a) : a) : ret);
                resolve(r);
            };
            if (typeof p.order != "string")
                return rr();
            let money = parseInt(p.money.toString()) / 100;
            if (!(money > 0))
                money = 0;
            if (!money) {
                let t = yield this.query(p.order);
                if (t.check)
                    money = t.payment;
            }
            if (!money)
                return rr();
            if (typeof p.orderT != "string")
                p.orderT = method.randStr("al");
            const q = {
                method: "refund",
                out_trade_no: p.order,
                refund_amount: money,
                refund_reason: p.title,
                out_request_no: p.orderT,
                notify_url: p.backUrl ? p.backUrl : this.config.notifyUrl,
            };
            resolve(yield this.orderMethod(q));
        }));
    }
    ;
    notify(req) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            let ret = { err: "验签失败", raw: req };
            if (this.alipay.checkNotifySign(req)) {
                delete req.sign;
                ret = req;
            }
            const r = new struct_1.REQUEST(ret);
            resolve(r);
        }));
    }
    ;
    front(order) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.query(order);
        });
    }
}
exports.PAY = PAY;
exports.default = PAY;
