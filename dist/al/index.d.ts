import Alipay from "alipay-sdk";
import { oa } from "../itf";
import { ORDER, REQUEST } from "../struct";
import * as itf from "./itf";
export declare class PAY {
    type: string[];
    config: itf.Config;
    alipay: Alipay;
    constructor(param: itf.Create);
    private Request;
    private orderMethod;
    create(p: ORDER): Promise<REQUEST>;
    query(order: string): Promise<REQUEST>;
    close(order: string): Promise<REQUEST>;
    back(p: ORDER): Promise<REQUEST>;
    notify(req: oa): Promise<REQUEST>;
    front(order: string): Promise<REQUEST>;
}
export default PAY;
