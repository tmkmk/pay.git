import { fun } from "../itf";
/** 银联涉及到的URL
 * @param front: string; // 前台交易请求地址
 * @param singleQuery: string; // 单笔查询请求地址
 * @param card: string; // 后台交易请求地址(若为有卡交易配置该地址)
 * @param back: string; // 后台交易请求地址(无卡交易配置该地址)
 * @param batch: string; // 批量交易请求地址
 * @param file: string; // 文件传输类交易地址
 * @param app: string; // APP交易请求地址
 * @param order: string; // 不知道什么玩意的地址,反正先放着
 */
export interface YLURL {
    front: string;
    singleQuery: string;
    card: string;
    back: string;
    batch: string;
    file: string;
    app: string;
    order: string;
}
/** 证书信息
 * @param id: string; // 证书id 为了美观随便放一个,无用
 * @param cert: string; // 证书信息
 * @param key: string; // 证书秘钥
 */
export interface Private {
    id: string;
    cert: string;
    key: string;
}
/** 创建实例所需参数成员如下:
 * @see Private
 * @see YLURL
 * @param private: Private; // 私钥证书
 * @param encryptCert: string; // 加密证书
 * @param middleCert: string; // 验签中级证书
 * @param rootCert: string; // 验签根证书
 * @param merId: string; // 商户id
 * @param frontUrl: string; // 前台通知地址
 * @param backUrl: string; // 后台通知地址
 * @param url: YLURL;
 */
export interface Create {
    appId: string;
    private: Private;
    encryptCert: string;
    middleCert: string;
    rootCert: string;
    frontUrl: string;
    backUrl: string;
    url: YLURL;
}
/** 保存在内部的配置信息,在Create基础上增加如下成员:
 * @see Create 包含此接口
 * @param version: string; // 版本
 * @param encoding: string; // 编码 固定的
 * @param signMethod: string; // 签名方式,证书方式固定01
 */
export interface Config extends Create {
    merId: string;
    version: string;
    encoding: string;
    signMethod: string;
}
export interface pOrder {
    money: string;
    title: string;
    order?: string;
    txnTime?: string;
    payTimeout?: string;
    accessType?: string;
    frontUrl?: string;
    backUrl?: string;
    channelType?: string;
    txnSubType?: string;
    realPay?: fun;
}
/** 查询公共参数
 * @param version: string; // 版本号
 * @param encoding: string;  // 编码方式
 * @param signMethod: string; // 签名方法
 * @param txnType: string; // 交易类型
 * @param txnSubType: string; // 交易子类
 * @param bizType: string; // 业务类型
 * @param accessType: string; // 接入类型
 * @param channelType: string; // 渠道类型
 * @param orderId: string; // 请修改被查询的交易的订单号，8-32位数字字母，不能含“-”或“_”
 * @param merId: string; // 商户id
 * @param txnTime: string; // 被查询的交易的订单发送时间，格式为YYYYMMDDhhmmss
 */
export interface QueryPublic {
    version: string;
    encoding: string;
    signMethod: string;
    txnType: string;
    txnSubType: string;
    bizType: string;
    accessType: string;
    channelType: string;
    orderId: string;
    merId: string;
    txnTime: string;
    [propName: string]: any;
}
export interface ORDER extends QueryPublic {
    frontUrl: string;
    backUrl: string;
    currencyCode: string;
    txnAmt: string;
    payTimeout: string;
    riskRateInfo: string;
    certId?: string;
    signature?: string;
}
export interface Query extends QueryPublic {
}
export interface QR {
    queryId: string;
    settleAmt: string;
    respCode: string;
    orderId: string;
    [propName: string]: any;
}
export interface Back extends QueryPublic {
    backUrl: string;
    origQryId: string;
    txnAmt: string;
}
