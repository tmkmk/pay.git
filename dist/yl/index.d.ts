import { oa } from "../itf";
import { REQUEST, ORDER } from "../struct";
import * as itf from "./itf";
export declare class PAY {
    config: itf.Config;
    constructor(param: itf.Create);
    create(p: ORDER, debug?: any): REQUEST;
    private get txnTime();
    private sign;
    private getSign;
    private createForm;
    private signStr;
    private qsparse;
    private validate;
    query(param: ORDER | string): Promise<REQUEST>;
    private revoke;
    back(param: ORDER | oa): Promise<REQUEST>;
    notify(req: oa): Promise<REQUEST>;
    front(req: oa): REQUEST;
}
export default PAY;
