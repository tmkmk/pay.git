"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PAY = void 0;
const method = __importStar(require("@mac-xiang/method"));
const crypto = __importStar(require("crypto"));
const axios_1 = __importDefault(require("axios"));
const struct_1 = require("../struct");
class PAY {
    constructor(param) {
        this.config = Object.assign({
            version: "5.1.0",
            encoding: "utf-8",
            signMethod: "01",
            merId: param.appId
        }, param);
    }
    create(p, debug) {
        let time = +new Date;
        if (!p.frontUrl)
            p.frontUrl = this.config.frontUrl;
        if (!p.backUrl)
            p.backUrl = this.config.backUrl;
        if (p.channelType != "08")
            p.channelType = "07";
        if (!p.accessType)
            p.accessType = "0";
        if (!p.order)
            p.order = method.randStr("yl");
        if (!p.txnTime)
            p.txnTime = method.formatDate(time, "yyyyMMDDHHmmss");
        time += 300000;
        if (!p.payTimeout)
            p.payTimeout = method.formatDate(time, "yyyyMMDDHHmmss");
        if (!p.title)
            p.title = "支付定金";
        const d = {
            version: this.config.version,
            encoding: this.config.encoding,
            signMethod: this.config.signMethod,
            txnType: "01",
            txnSubType: "01",
            bizType: "000201",
            frontUrl: p.frontUrl,
            backUrl: p.backUrl,
            channelType: p.channelType,
            accessType: p.accessType,
            currencyCode: "156",
            merId: this.config.merId,
            orderId: p.order,
            txnTime: p.txnTime,
            txnAmt: p.money.toString(),
            payTimeout: p.payTimeout,
            riskRateInfo: `{commodityName=${p.title}}`,
        };
        this.sign(d);
        p.order = d.orderId;
        p.channelType = d.channelType;
        p.txnTime = d.txnTime;
        p.respCode = "00";
        p.body = this.createForm(d, this.config.url.front, "post", debug);
        return new struct_1.REQUEST(p);
    }
    get txnTime() {
        return method.formatDate(new Date, "yyyyMMDDHHmmss");
    }
    sign(p) {
        delete p.signature;
        let ret = "";
        switch (p.signMethod) {
            case "01": // 固定为01
                p.certId = this.config.private.id;
                ret = this.getSign(p, this.config.private.key);
                break;
            case "11": // 支持散列方式验证SHA-256 官网说明文档不够清晰.
                p.certId = this.config.private.id;
                ret = this.getSign(p, this.config.private.key);
                break;
            case "12": // 支持散列方式验证SM3 官网文档太过操蛋
                break;
            default:
                break;
        }
        return ret;
    }
    getSign(p, privateKey) {
        const ps = this.signStr(p);
        const signStr = crypto.createHash('sha256').update(ps).digest("hex");
        switch (p.signMethod) {
            case "01":
                p.signature = crypto.createSign("sha256")
                    .update(signStr, "utf8").sign(privateKey, "base64");
                break;
            case "11": // 没用
                console.log("散列算法");
                p.signature = crypto.createHash("sha256")
                    .update(ps)
                    .digest("hex");
                break;
            default:
                break;
        }
        // console.log(qs.stringify(p));
        // return qs.stringify(p);
        return ps + "&signature=" + encodeURIComponent(p.signature);
    }
    createForm(p, url, formMethod = "post", debug) {
        // const fid = method.randStr("");
        let r = "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head>";
        r += debug ? `<body>` : `<body onload="javascript:document.pay_form.submit();">`;
        r += `<form id="pay_form" name="pay_form" action="${url}" method="${formMethod}">\n`;
        Object.keys(p).forEach(k => {
            r += `<input type="hidden" name="${k}" id="${k}" value="${p[k]}" />\n`;
        });
        if (debug)
            r += `<button onclick="javascript:document.pay_form.submit();">提交</button>`;
        r += "</form></body></html>";
        return r;
    }
    signStr(p) {
        return Object.keys(p).sort().map((k) => {
            return `${k}=${p[k]}`;
        }).join("&");
    }
    qsparse(p) {
        let s = p.replace(/\r/g, ""), ret = {};
        p.split("&").forEach(a => {
            let i = a.indexOf("=");
            ret[a.substr(0, i++)] = a.substr(i);
        });
        return ret;
    }
    validate(param) {
        let ret = { err: "数据错误", raw: param };
        const p = typeof param == "string" ? this.qsparse(param) : Object.assign({}, param);
        if (p.signature) {
            const signature = p.signature;
            delete p.signature;
            const pk = "-----BEGIN CERTIFICATE-----\r\nMIIEYzCCA0ugAwIBAgIFEDkwhTQwDQYJKoZIhvcNAQEFBQAwWDELMAkGA1UEBhMC\r\nQ04xMDAuBgNVBAoTJ0NoaW5hIEZpbmFuY2lhbCBDZXJ0aWZpY2F0aW9uIEF1dGhv\r\ncml0eTEXMBUGA1UEAxMOQ0ZDQSBURVNUIE9DQTEwHhcNMjAwNzMxMDExOTE2WhcN\r\nMjUwNzMxMDExOTE2WjCBljELMAkGA1UEBhMCY24xEjAQBgNVBAoTCUNGQ0EgT0NB\r\nMTEWMBQGA1UECxMNTG9jYWwgUkEgT0NBMTEUMBIGA1UECxMLRW50ZXJwcmlzZXMx\r\nRTBDBgNVBAMMPDA0MUA4MzEwMDAwMDAwMDgzMDQwQOS4reWbvemTtuiBlOiCoeS7\r\nveaciemZkOWFrOWPuEAwMDAxNjQ5NTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCC\r\nAQoCggEBAMHNa81t44KBfUWUgZhb1YTx3nO9DeagzBO5ZEE9UZkdK5+2IpuYi48w\r\neYisCaLpLuhrwTced19w2UR5hVrc29aa2TxMvQH9s74bsAy7mqUJX+mPd6KThmCr\r\nt5LriSQ7rDlD0MALq3yimLvkEdwYJnvyzA6CpHntP728HIGTXZH6zOL0OAvTnP8u\r\nRCHZ8sXJPFUkZcbG3oVpdXQTJVlISZUUUhsfSsNdvRDrcKYY+bDWTMEcG8ZuMZzL\r\ng0N+/spSwB8eWz+4P87nGFVlBMviBmJJX8u05oOXPyIcZu+CWybFQVcS2sMWDVZy\r\nsPeT3tPuBDbFWmKQYuu+gT83PM3G6zMCAwEAAaOB9DCB8TAfBgNVHSMEGDAWgBTP\r\ncJ1h6518Lrj3ywJA9wmd/jN0gDBIBgNVHSAEQTA/MD0GCGCBHIbvKgEBMDEwLwYI\r\nKwYBBQUHAgEWI2h0dHA6Ly93d3cuY2ZjYS5jb20uY24vdXMvdXMtMTQuaHRtMDkG\r\nA1UdHwQyMDAwLqAsoCqGKGh0dHA6Ly91Y3JsLmNmY2EuY29tLmNuL1JTQS9jcmw3\r\nNTAwMy5jcmwwCwYDVR0PBAQDAgPoMB0GA1UdDgQWBBTmzk7XEM/J/sd+wPrMils3\r\n9rJ2/DAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwQwDQYJKoZIhvcNAQEF\r\nBQADggEBAJLbXxbJaFngROADdNmNUyVxPtbAvK32Ia0EjgDh/vjn1hpRNgvL4flH\r\nNsGNttCy8afLJcH8UnFJyGLas8v/P3UKXTJtgrOj1mtothv7CQa4LUYhzrVw3UhL\r\n4L1CTtmE6D1Kf3+c2Fj6TneK+MoK9AuckySjK5at6a2GQi18Y27gVF88Nk8bp1lJ\r\nvzOwPKd8R7iGFotuF4/8GGhBKR4k46EYnKCodyIhNpPdQfpaN5AKeS7xeLSbFvPJ\r\nHYrtBsI48jUK/WKtWBJWhFH+Gty+GWX0e5n2QHXHW6qH62M0lDo7OYeyBvG1mh9u\r\nQ0C300Eo+XOoO4M1WvsRBAF13g9RPSw=\r\n-----END CERTIFICATE-----";
            const publicKey = p.signPubKeyCert;
            // .replace(/ /g, "+")
            // .replace(/BEGIN\+/, "BEGIN ")
            // .replace(/END\+/, "END ");
            const params_sha256x16 = crypto.createHash("sha256")
                .update(this.signStr(p))
                .digest("hex");
            const verifier = crypto.createVerify('RSA-SHA256');
            verifier.update(Buffer.from(params_sha256x16, 'utf-8'));
            let result;
            try {
                result = verifier.verify(publicKey, signature, "base64");
            }
            catch (error) {
                console.log(error.stack);
            }
            if (result) {
                // console.log("验签通过", publicKey == pk);
                delete p.signPubKeyCert;
                ret = p;
            }
            else {
                ret = { err: "验签失败", raw: param };
            }
            // console.log(signature);
        }
        return new struct_1.REQUEST(ret);
    }
    query(param) {
        return new Promise((resolve) => {
            const p = new struct_1.ORDER(typeof param == "string" ? { order: param } : param);
            let r = new struct_1.REQUEST({ err: "参数不正确", raw: p });
            if (!p.order)
                return r;
            if (!p.txnTime)
                p.txnTime = this.txnTime;
            if (p.channelType !== "07")
                p.channelType = "08";
            const d = {
                version: this.config.version,
                encoding: this.config.encoding,
                signMethod: this.config.signMethod,
                txnType: "00",
                txnSubType: "00",
                bizType: "000000",
                accessType: "0",
                channelType: p.channelType,
                orderId: p.order,
                merId: this.config.merId,
                txnTime: p.txnTime ? p.txnTime : this.txnTime, // 被查询的交易的订单发送时间，格式为YYYYMMDDhhmmss
            };
            // console.log(this.config.url.singleQuery);
            axios_1.default({
                method: "post",
                url: this.config.url.singleQuery,
                data: this.sign(d),
            }).then(r => {
                resolve(this.validate(r.data));
            }).catch(e => {
                resolve(new struct_1.REQUEST({ err: "网络错误", raw: e }));
            });
        });
    }
    revoke(p) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            if (!p.frontUrl)
                p.frontUrl = this.config.frontUrl;
            if (!p.backUrl)
                p.backUrl = this.config.backUrl;
            if (p.channelType != "08")
                p.channelType = "07";
            if (!p.accessType)
                p.accessType = "0";
            if (!p.txnTime)
                p.txnTime = this.txnTime;
            if (!p.title)
                p.title = "支付定金";
            const q = {
                version: this.config.version,
                encoding: this.config.encoding,
                signMethod: this.config.signMethod,
                txnType: "31",
                txnSubType: "00",
                bizType: "000201",
                accessType: "0",
                channelType: p.channelType,
                backUrl: p.backUrl,
                orderId: p.orderT,
                merId: this.config.merId,
                origQryId: p.queryId,
                txnTime: p.txnTime,
                txnAmt: p.money.toString(),
                reqReserved: JSON.stringify({ order: p.order })
            };
            axios_1.default({
                method: "post",
                url: this.config.url.back,
                data: this.sign(q)
            }).then(r => {
                const res = this.validate(r.data);
                resolve(res);
            }).catch(e => {
                resolve(new struct_1.REQUEST({ err: "网络错误", raw: e }));
            });
        }));
    }
    back(param) {
        return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
            const p = new struct_1.ORDER(param);
            if (!p.order)
                return resolve(new struct_1.REQUEST({ err: "参数错误", raw: p }));
            if (!(p.money > 0))
                p.money = 0;
            if (!p.orderT)
                p.orderT = method.randStr("ylt"); // 生成新订单
            if (!p.frontUrl)
                p.frontUrl = this.config.frontUrl;
            if (!p.backUrl)
                p.backUrl = this.config.backUrl;
            if (p.channelType != "08")
                p.channelType = "07";
            if (!p.accessType)
                p.accessType = "0";
            if (!p.txnTime)
                p.txnTime = this.txnTime;
            if (!p.title)
                p.title = "退还定金";
            if (!p.queryId || !p.money) { // 确认流水号 总金额
                const o = yield this.query(p);
                if (o.check) {
                    p.queryId = o.raw.queryId;
                    if (!p.money) {
                        p.money = o.payment;
                        // const res = await this.revoke(p); // 撤销交易
                        // console.log(res);
                        // if (res.err) {
                        //   if (res.raw.respMsg) Object.assign(res, { err: res.raw.respMsg });
                        //   resolve(res);
                        //   return;
                        // } else return resolve(res); // 撤销成功
                    }
                }
                else
                    return resolve(Object.assign(o, { err: "退款失败" }));
            }
            // resolve(new REQUEST({ err: "手动结束" }));
            const q = {
                version: this.config.version,
                encoding: this.config.encoding,
                signMethod: this.config.signMethod,
                txnType: "04",
                txnSubType: "00",
                bizType: "000201",
                accessType: p.accessType,
                channelType: p.channelType,
                backUrl: p.backUrl,
                orderId: p.orderT,
                merId: this.config.merId,
                origQryId: p.queryId,
                txnTime: p.txnTime,
                txnAmt: p.money.toString(),
                reqReserved: JSON.stringify({ order: p.order })
            };
            // console.log(q);
            // return resolve(new REQUEST({ err: "手动结束" }));
            axios_1.default({
                method: "post",
                url: this.config.url.back,
                data: this.sign(q)
            }).then(r => {
                // resolve(new REQUEST("结束"));
                const res = this.validate(r.data);
                res.order = p.order;
                if (res.raw.respMsg)
                    Object.assign(res, { err: res.raw.respMsg });
                resolve(res);
            }).catch(e => {
                resolve(new struct_1.REQUEST({ err: "网络错误", raw: e }));
            });
        }));
    }
    notify(req) {
        return __awaiter(this, void 0, void 0, function* () {
            let res = yield this.validate(req);
            if (res.check && typeof res.raw.reqReserved == "string") {
                res.raw.reqReserved = JSON.parse(res.raw.reqReserved);
            }
            return res;
        });
    }
    front(req) {
        return this.validate(req);
    }
}
exports.PAY = PAY;
exports.default = PAY;
