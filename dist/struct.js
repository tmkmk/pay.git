"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.REQUEST = exports.ORDER = void 0;
class ORDER {
    constructor(param) {
        // 公共
        this.money = 0; // 交易金额 单位为【分】
        this.title = ""; // 商品名称
        this.order = ""; // 商户订单号
        Object.assign(this, param);
        this.money = param.money ? param.money : 0;
        this.title = param.title ? param.title.toString() : "   ";
    }
}
exports.ORDER = ORDER;
class REQUEST {
    constructor(p) {
        this.order = ""; // 商户订单号
        this.payment = 0; // 金额,单位分
        this.check = false;
        if (p instanceof REQUEST) {
            Object.assign(this, p);
            return;
        }
        if (!p)
            p = { err: "数据为空", raw: p };
        this.order = p.order || p.orderId || p.out_trade_no || "";
        if (typeof p.queryId == "string")
            this.queryId = p.queryId;
        if (p.err) {
            this.err = p.err;
            const t = Object.assign({}, p);
            delete t.err;
            this.raw = t.raw ? t.raw : t;
        }
        else {
            if (typeof p.code_url == "string" || typeof p.url == "string")
                this.url = p.url || p.code_url;
            this.check =
                p.respCode == "00" ||
                    p.result_code == "SUCCESS" ||
                    p.err_code_des == "订单已全额退款" ||
                    p.refund_status == "SUCCESS" ||
                    p.trade_status == "TRADE_SUCCESS" || // 支付宝
                    p.msg == "Success" ||
                    p.sub_code == "ACQ.TRADE_HAS_CLOSE" ||
                    false;
            if (this.check) {
                this.payment = p.money || p.settleAmt || p.cash_fee || p.total_fee ||
                    this.toNumber(p.total_amount, 100) || this.toNumber(p.refund_fee, 100) ||
                    p.txnAmt ||
                    0;
                this.payment = this.toNumber(this.payment);
                if (typeof p.body == "string")
                    this.body = p.body;
                if (typeof p.app)
                    this.app = p.app;
            }
            else
                this.err = "订单未完成";
            this.raw = p;
        }
    }
    toNumber(p, multiple = 1) {
        let r = Number(p);
        r = r ? r : 0;
        return parseInt((r * multiple).toString());
    }
}
exports.REQUEST = REQUEST;
