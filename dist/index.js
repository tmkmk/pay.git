"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PAY = exports.REQUEST = exports.ORDER = void 0;
const fs = __importStar(require("fs"));
const path = __importStar(require("path"));
const struct_1 = require("./struct");
Object.defineProperty(exports, "ORDER", { enumerable: true, get: function () { return struct_1.ORDER; } });
Object.defineProperty(exports, "REQUEST", { enumerable: true, get: function () { return struct_1.REQUEST; } });
const yl_1 = __importDefault(require("./yl"));
const wx_1 = __importDefault(require("./wx"));
const al_1 = __importDefault(require("./al"));
function resolve(dir) {
    return path.resolve(__dirname, dir); //path.join(__dirname)设置绝对路径 
}
function readFile(filePath, options) {
    return fs.readFileSync(resolve(filePath), options);
}
class PAY {
    constructor(config) {
        this.al = new al_1.default(config.al);
        this.wx = new wx_1.default(config.wx);
        this.yl = new yl_1.default(config.yl);
        // this.handle = setInterval(() => { // 设置响应超时
        //   const time = new Date().getTime();
        //   Object.keys(this.cache.callback).forEach(k => {
        //     if (!(time < this.cache.callback[k].time)) {
        //       const fun = this.cache.callback[k].fun;
        //       delete this.cache.callback[k];
        //       try {
        //         fun(new REQUEST({ err: "响应超时" }));
        //       } catch (error) { }
        //     }
        //   });
        //   Object.keys(this.cache.orderPaymentPage).forEach(k => {
        //     if (!(time < this.cache.orderPaymentPage[k].time)) {
        //       delete this.cache.orderPaymentPage[k];
        //     }
        //   });
        // }, 10000);
    }
    ;
}
exports.PAY = PAY;
