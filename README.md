# tmkPay

#### 介绍

一个基于 ts 的后端支付 npm 库.第一次写各种不规范请指出.

#### 软件架构

```
├─dist                ts编译后存放路径,其结构基本与src一致,没啥好看的
├─src                 源码目录
│  └─yl               银联支付
|     ├─index.ts      银联支付主要代码
|     ├─itf.ts        银联支付用到的interface
|     ├─index.php     最终选择了使用php来实现读取银联证书,dist中没有此文件.如要使用请手动放进去.
|     └─itf.ts        银联支付用到的interface
|─test                测试开发目录
|  |─acp             放银联测试证书的目录
|  |─dev.ts          测试用代码很少一看就懂
│  └─其他文件         没啥说的.偶尔测试会用
```

#### 安装教程

npm i @max-xiang/pay

#### 使用说明

1.  使用银联支付时,从银联得到的 pfx 整数请使用 index.php 或其他工具进行解码后将数据放入配置参数中.需要用到 整数 id,以及私钥.如果系统环境可以执行 php -f 命令也可以传值 privatePath 为证书绝对路径,将自动执行 index.php 解密.之所以这么麻烦,是因为 Node/Python/java 不但需要安装本身环境,还需要额外下载并安装 OpenSSL 才能解码.比较麻烦,而 PHP 则直接可以使用.至于 C 等,编译起来更麻烦. 以后可能会考虑使用 C 动态链接库的方式加入到里面来.
2.  微信证书请参考银联证书部分.
3.  阿里支付嘛~!直接采用公钥私钥就行了.别使用证书的方法.
4.  具体的说明,请参考 src 目录中的 itf.ts 注释.

#### 参与贡献

就我一个,但使用了 n 多包.

#### 特技

npm 上传包命令
npm publish --registry http://registry.npmjs.org
npm config set registry https://registry.npmjs.org
npm config set registry https://registry.npm.taobao.org
npm publish --access public --registry http://registry.npmjs.org
npm config set scope mac-xiang --global

npm version prerelease

npm version prepatch
别笑,记不住

### 更新日志
v1.0.3-2:
完全测试支付宝支付 APP+WEB.
v1.0.3-1:
修复支付宝支付APP与网页支付不兼容.
v1.0.2-1:
修复微信支付 bug.非原生支付时,允许传值 trade_type,openid.用以特殊情况下的支付.例如 app 支付
v1.0.2-0:
微信支付在创建订单时,加入可选参数 openid
