'use strict';
const expect = require('chai').expect;
const add = require('../dist/index').add;

describe('ts-hi function test', () => {
  it('should return 3', () => {
    const result = add(1, 2);
    expect(result).to.equal(3);
  });
});