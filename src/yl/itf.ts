import { fun } from "../itf";
/** 银联涉及到的URL
 * @param front: string; // 前台交易请求地址
 * @param singleQuery: string; // 单笔查询请求地址
 * @param card: string; // 后台交易请求地址(若为有卡交易配置该地址)
 * @param back: string; // 后台交易请求地址(无卡交易配置该地址)
 * @param batch: string; // 批量交易请求地址
 * @param file: string; // 文件传输类交易地址
 * @param app: string; // APP交易请求地址
 * @param order: string; // 不知道什么玩意的地址,反正先放着
 */
export interface YLURL {
  front: string; // 前台交易请求地址
  singleQuery: string; // 单笔查询请求地址
  card: string; // 后台交易请求地址(若为有卡交易配置该地址)
  back: string; // 后台交易请求地址(无卡交易配置该地址)
  batch: string; // 批量交易请求地址
  file: string; // 文件传输类交易地址
  app: string; // APP交易请求地址
  order: string; // 不知道什么玩意的地址,反正先放着
}
/** 证书信息
 * @param id: string; // 证书id 为了美观随便放一个,无用
 * @param cert: string; // 证书信息
 * @param key: string; // 证书秘钥
 */
export interface Private {
  id: string; // 证书id 为了美观随便放一个,无用
  cert: string; // 证书信息
  key: string; // 证书秘钥
}
/** 创建实例所需参数成员如下:
 * @see Private
 * @see YLURL
 * @param private: Private; // 私钥证书
 * @param encryptCert: string; // 加密证书
 * @param middleCert: string; // 验签中级证书
 * @param rootCert: string; // 验签根证书
 * @param merId: string; // 商户id
 * @param frontUrl: string; // 前台通知地址
 * @param backUrl: string; // 后台通知地址
 * @param url: YLURL;
 */
export interface Create {
  appId: string; // 商户id
  private: Private; // 私钥证书
  encryptCert: string; // 加密证书
  middleCert: string; // 验签中级证书
  rootCert: string; // 验签根证书
  frontUrl: string; // 前台通知地址
  backUrl: string; // 后台通知地址
  url: YLURL;
}
/** 保存在内部的配置信息,在Create基础上增加如下成员:
 * @see Create 包含此接口
 * @param version: string; // 版本
 * @param encoding: string; // 编码 固定的
 * @param signMethod: string; // 签名方式,证书方式固定01
 */
export interface Config extends Create {
  merId: string; // 商户id
  version: string; // 版本
  encoding: string; // 编码 固定的
  signMethod: string; // 签名方式,证书方式固定01
}

export interface pOrder {
  money: string; // 交易金额 单位为【分】
  title: string; // 商品名称
  order?: string; // 商户订单号
  txnTime?: string;
  payTimeout?: string;
  accessType?: string; // "0" | "1" | "3"; // 0：商户直连接入;1：收单机构接入;2：平台商户接入
  frontUrl?: string; // 前台通知地址
  backUrl?: string; // 后台通知地址
  channelType?: string; // "07" | "08"; // 渠道类型，07-PC，08-手机
  txnSubType?: string; // 01：自助消费，通过地址的方式区分前台消费和后台消费（含无跳转支付） 03：分期付款
  realPay?: fun;
}

/** 查询公共参数
 * @param version: string; // 版本号
 * @param encoding: string;  // 编码方式
 * @param signMethod: string; // 签名方法
 * @param txnType: string; // 交易类型
 * @param txnSubType: string; // 交易子类
 * @param bizType: string; // 业务类型
 * @param accessType: string; // 接入类型
 * @param channelType: string; // 渠道类型
 * @param orderId: string; // 请修改被查询的交易的订单号，8-32位数字字母，不能含“-”或“_”
 * @param merId: string; // 商户id
 * @param txnTime: string; // 被查询的交易的订单发送时间，格式为YYYYMMDDhhmmss
 */
export interface QueryPublic {
  version: string; // 版本号
  encoding: string;  // 编码方式
  signMethod: string; // 签名方法
  txnType: string; // 交易类型
  txnSubType: string; // 交易子类
  bizType: string; // 业务类型
  accessType: string; // 接入类型
  channelType: string; // 渠道类型
  orderId: string; // 请修改被查询的交易的订单号，8-32位数字字母，不能含“-”或“_”
  merId: string; // 商户id
  txnTime: string; // 被查询的交易的订单发送时间，格式为YYYYMMDDhhmmss
  [propName: string]: any;
}
export interface ORDER extends QueryPublic {
  frontUrl: string;
  backUrl: string;
  currencyCode: string;
  txnAmt: string;
  payTimeout: string;
  riskRateInfo: string;
  certId?: string;
  signature?: string;
}
export interface Query extends QueryPublic {

}
export interface QR {
  queryId: string; // 流水号.退款时要用这操蛋玩意
  settleAmt: string; // 金额,单位分
  respCode: string; // 应答码 00 表示成功
  orderId: string; // 商户订单号
  [propName: string]: any;
}

export interface Back extends QueryPublic {
  backUrl: string; // 后台通知地址
  origQryId: string; // 原消费的queryId，可以从查询接口或者通知接口中获取
  txnAmt: string; // 交易金额
}
