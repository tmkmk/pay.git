
import * as fs from "fs";
import * as path from "path";
import stripJsonComments from "strip-json-comments";

import { PayParm } from "./itf";
import { ORDER, REQUEST } from "./struct";
export { ORDER, REQUEST };
  

import YL from "./yl";
import WX from "./wx";
import AL from "./al";

function resolve(dir: string): string {
  return path.resolve(__dirname, dir);//path.join(__dirname)设置绝对路径 
}
function readFile(filePath: string, options?: any) {
  return fs.readFileSync(resolve(filePath), options);
}

export class PAY {
  al: AL;
  wx: WX;
  yl: YL;
  constructor (config: PayParm) {
    this.al = new AL(config.al);
    this.wx = new WX(config.wx);
    this.yl = new YL(config.yl);

    // this.handle = setInterval(() => { // 设置响应超时
    //   const time = new Date().getTime();
    //   Object.keys(this.cache.callback).forEach(k => {
    //     if (!(time < this.cache.callback[k].time)) {
    //       const fun = this.cache.callback[k].fun;
    //       delete this.cache.callback[k];
    //       try {
    //         fun(new REQUEST({ err: "响应超时" }));
    //       } catch (error) { }
    //     }
    //   });
    //   Object.keys(this.cache.orderPaymentPage).forEach(k => {
    //     if (!(time < this.cache.orderPaymentPage[k].time)) {
    //       delete this.cache.orderPaymentPage[k];
    //     }
    //   });
    // }, 10000);
  };

}
