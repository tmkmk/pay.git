import * as yl from "./yl/itf";
import * as wx from "./wx/itf";
import * as al from "./al/itf";

export type ns = number | string;
export interface oa {
  [propName: string]: any;
}
export interface sob {
  [propName: string]: ns;
}
export interface convertableToString { toString(): string; }
export interface fun { (param: any): any; }
export interface Callback { [propName: string]: { time: number, fun: fun; }; }
export interface OrderPaymentPage {
  [prop: string]: { time: number, body: string; };
}
export interface Cache {
  orderPaymentPage: OrderPaymentPage;
  callback: Callback;
}

export interface PayParm {
  wx: wx.Create;
  al: al.Create;
  yl: yl.Create;
  cache?: Cache;
}

