/** 创建实例所需参数成员:
 * @appId: string; // 支付宝下发的appid
 * @privateKey: string; // 应用私钥
 * @publicKey: string; // 支付宝公钥 上传后生成的那一串,不是上传前的那一串
 * @gateway: string; // 应用网关, 注意区分沙箱/正式 环境
 * @notifyUrl: string; // 默认异步通知地址
 * @payUrl: string; // 支付地址
 */
export interface Create {
  appId: string; // 支付宝下发的appid
  privateKey: string; // 应用私钥
  publicKey: string; // 支付宝公钥 上传后生成的那一串,不是上传前的那一串
  gateway: string; // 应用网关, 注意区分沙箱/正式 环境
  notifyUrl: string; // 默认异步通知地址
  payUrl?: string;
  encryptKey?: string; // 
}
/** 实例内部config选项.与create一致
 * @see Create 包含此接口
 */
export interface Config extends Create { }
