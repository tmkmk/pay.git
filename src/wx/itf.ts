/** 详细看接口备注
 */
export interface Create {
  appId: string; // 微信下发的appid
  mchId: string; // 商户id
  partnerKey: string; // API密钥
  notifyUrl?: string; // 默认异步通知地址
  private: { // 证书相关
    id?: string; // 证书id 为了美观随便放一个,无用
    cert: string; // 证书信息
    key: string; // 证书秘钥
  };
}
export interface Config {
  partner_key: string;
  notify_url: string;
  public: {
    appid: string;
    mch_id: string;
  };
  private: { // 证书相关
    id?: string; // 证书id 为了美观随便放一个,无用
    cert: string; // 证书信息
    key: string; // 证书秘钥
  };
}
